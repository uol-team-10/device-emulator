import serial

PORT = '/dev/ttyS4'     # CHECK PORT DEVICE IS CONNECTED TO
                        # IN WSL, COM# BECOMES /dev/ttyS#

BAUDRATE = 9600        # CHECK BAUDRATE MATCHES DEVICE

DATAFILE = 'data'

while True:
    try:
        ser = serial.Serial(port=PORT, baudrate=BAUDRATE)

        while True:
            data = ser.readline().decode('ASCII').strip()

            print(data)
            with open(DATAFILE, 'a+') as f:
                f.write(data)

    except KeyboardInterrupt:
        print('Keyboard interrupt, exiting script...')
        break
    except serial.serialutil.SerialException:
        print('Device disconnected, exiting script...')
        break

# CT1 CT2 CT3 CT4 VRMS/BATT PULSE