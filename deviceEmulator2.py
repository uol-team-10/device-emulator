import http
import urllib3
import time
import re
import requests
import json
from datetime import datetime

PUBLISH_RATE = 1    # Change the rate at which data is sent to the server

DEVICE_ID = 'emulatorxxxxxxxxxxxxxxxxxxxxxxxx'
HOST = 'localhost'
PORT = 5000
ENDPOINT = '/devices/{deviceID}/telemetry'.format(deviceID=DEVICE_ID)
DATAFILE = 'data'


while True:
    try:
        with open(DATAFILE) as f:
            for line in f:
                try:
                    data = json.loads(line)
                    ts = int(time.time())
                    data['ts'] = ts
                    r = requests.post(
                        url='http://{host}:{port}{endpoint}'.format(host=HOST,port=PORT,endpoint=ENDPOINT), 
                        json=data
                    )
                    status = r.status_code
                    print('{time}: {status}'.format(time=ts,status=status))
                    # print(line)
                    # print(json.dumps(data).replace(' ',''))
                    time.sleep(PUBLISH_RATE)
                except http.client.HTTPException:
                    print('HTTP Exception')
                    pass
                except http.client.RemoteDisconnected:
                    print('Remote Disconnected error')
                    pass
                except urllib3.exceptions.ProtocolError:
                    print ('Protocol error')
                    pass
                except requests.exceptions.ConnectionError:
                    print('Connection error')
                    pass
                except ConnectionResetError:
                    print('Connection reset error')
                    pass
    except KeyboardInterrupt:
        print('Keyboard interrupt, exiting script...')
        break